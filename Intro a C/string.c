#include <stdio.h>
#include <string.h>
#define TAMANO_STR 100

int main(){
	char letra = 97;
	char *mensaje = "hola mundo\n";

	//char mensaje2 = "incorrecto";  //incorrecto	
	//mensaje[1] = 'Q';	     //incorrecto

	char mutable[TAMANO_STR]; // = {'z'};
	memset(mutable, 'z', TAMANO_STR);

	mutable[0] = 'c';
	mutable[1] = 'o';
	mutable[2] = 'l';
	mutable[3] = 'a';

	
	printf("La letra es %d\n", letra);
	printf("El mensaje es %s\n", mensaje);
	printf("String mutable %s\n", mutable);

	char buf2[2000];
	printf("buf2: %s\n", buf2);

	return 0;

}
