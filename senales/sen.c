#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void manejar_senal(int signo){
	printf("Senal %d, =P\n",signo);
}

void manejar_senal_2(int signo){
        printf("Senal %d, =(\n",signo);
}


int main(){
	
	signal(SIGCONT, manejar_senal);
	signal(SIGINT, manejar_senal_2);
	unsigned int i= 0;
	while(1){
		printf("Contando... %d\n", i);
		sleep(1);
		i++;
	}

}
