#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>


void grid(int white_pos){
	char grid[4][52] = {0};
	printf("\e[1;1H\e[2J");
	for(int i = 0; i < 4; i++){
		memset(grid[i], 'x', 52);
		grid[i][0] = '\r';
		grid[i][51] = 0;
		grid[i][white_pos] = ' ';
		printf("%s", grid[i]);
		//printf("Tam %d\n", strlen(grid[i]));
		printf("\n");
		fflush(stdout);
	}

}


void grid2(int white_pos){
	char grid[4][51] = {0};
	printf("\e[1;1H\e[2J");
	for(int i = 0; i < 4; i++){
		memset(grid[i], ' ', 51);
		grid[i][51] = 0;
		grid[i][white_pos] = 'x';
		printf("%s", grid[i]);
		//printf("Tam %d\n", strlen(grid[i]));
		printf("\n");
		fflush(stdout);
	}

}

void dibujar_grilla(char **matriz, int fil, int col){		//0 -> vacio, 1 -> x
	printf("\e[1;1H\e[2J");
	char *linea = malloc(col + 1);	//Char nulo al final
	for(int i = 0; i < fil; i++){
		memset(linea, ' ', col+1);
		linea[col] = 0;
		for(int j = 0; j < col; j++){
			if(matriz[i][j] == 0){
				linea[j] = 'x';
			}
			else if(matriz[i][j] == 1){
				linea[j] = ' ';			
			}
		}
		printf("%s", linea);
		printf("\n");
		fflush(stdout);
	}

}

void generar_grilla(char **grilla, int fil, int col, int pos){

	if(pos >= col){
		return;	
	}

	for(int i = 0; i < fil; i++){
		memset(grilla[i], 0, col);	
		grilla[i][pos] = 1;
	}

}

long random_at_most(long max) {
  unsigned long
    // max <= RAND_MAX < ULONG_MAX, so this is okay.
    num_bins = (unsigned long) max,
    num_rand = (unsigned long) RAND_MAX,
    bin_size = num_rand / num_bins,
    defect   = num_rand % num_bins;

  long x;
  do {
   x = random();
  }
  // This is carefully written not to overflow
  while (num_rand - defect <= (unsigned long)x);

  // Truncated division is intentional
  return x/bin_size;
}

void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad){
	for(int i =0; i<fil; i++){
		memset(grilla[i], 0, col);	
	}
	srand(time(NULL));
	for(int i = 0; i < cantidad; i++){
		long rnd_fil = rand() % fil;	
		long rnd_col = rand() % col;
		
		printf("%ld %ld\n", rnd_fil, rnd_col);
		if(grilla[rnd_fil][rnd_col] == 1){
			i--;
		}
		else{
			grilla[rnd_fil][rnd_col] = 1;
		}
		
	}
}






int main(){
	int fil = 20;
	int col = 100;

	char **grilla = malloc(fil*sizeof(char*));
	for(int i = 0; i < fil; i++){
		grilla[i] = malloc(col);
	}


	int i = 10;
        while(1){
                //generar_grilla(grilla, fil, col, i);
		llenar_matriz_azar(grilla, fil, col, 10);
		dibujar_grilla(grilla, fil, col);
		printf("\n\n\n");
		printf("Valor de i: %d\n",i);
		i++;
		if( i % col == 0){
			i = 0;		
		}
		usleep(40000);
        }

	/*printf("                                          \n");
	printf("       x                                  \n");
	printf("                                  x x     \n");
	printf("                                   xx     \n");
	printf("         xx                        x      \n");
	printf("         xx                               \n");
	printf("                                          \n");
	printf("                                          \n");
	printf("                   x                      \n");
	printf("                   x                      \n");
	printf("                   x                      \n");*/
        return 0;

}






/*for(int i = 0; i < 50; i++){
                        char buf[52] = {0};
                        memset(buf, 'x', 50);
                        buf[0] = '\r';
                        if(i >= 1 && i <= 50){
                                buf[i] = ' ';
                        }
                        printf("%s", buf);
                        fflush(stdout);
                        usleep(100000);
                }
		printf("\n");*/
                //sleep(1);
