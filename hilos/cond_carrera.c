#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

int valor = 0;

void *rutina_hilo(void *args){

	for(int i = 0; i < 100; i++){
		valor++;
	}

	return (void *)0;
}

void *rutina2(void *args){
	for(int i = 0; i < 200; i++){
		valor--;
	}
	return NULL;
}

int main(){
	unsigned int id = pthread_self();
	printf("Id del hilo principal %d\n", id);

	pthread_t id_hilo;
	pthread_t id_hilo2;
	pthread_create(&id_hilo, NULL, rutina_hilo, NULL);

	pthread_create(&id_hilo2, NULL, rutina2, NULL);


	pthread_join(id_hilo, NULL);
	pthread_join(id_hilo2, NULL);
	printf("Los hilos trabajadores terminaron. valor = %d\n", valor);
	return 0;
}

