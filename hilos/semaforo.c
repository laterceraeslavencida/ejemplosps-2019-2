#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>

int valor = 0;

sem_t mutex;


void *rutina_hilo(void *args){

	for(int i = 0; i < 10000000; i++){
		sem_wait(&mutex);
		valor++;
		sem_post(&mutex);
	}

	return (void *)0;
}

void *rutina2(void *args){
	for(int i = 0; i < 20000000; i++){
		sem_wait(&mutex);
		valor--;
		sem_post(&mutex);
	}
	return NULL;
}

int main(){
	unsigned int id = pthread_self();
	printf("Id del hilo principal %d\n", id);

	sem_init(&mutex, 0, 1);
	pthread_t id_hilo;
	pthread_t id_hilo2;
	pthread_create(&id_hilo, NULL, rutina_hilo, NULL);

	pthread_create(&id_hilo2, NULL, rutina2, NULL);


	pthread_join(id_hilo, NULL);
	pthread_join(id_hilo2, NULL);
	printf("Los hilos trabajadores terminaron. valor = %d\n", valor);
	return 0;
}

