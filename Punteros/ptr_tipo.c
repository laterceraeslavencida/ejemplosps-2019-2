#include <stdio.h>


int main(){
	long a = 5000003348;
	long *p = &a;

	unsigned short *w = (unsigned short *)&a;

	printf("%p: %d\n", w, *w);
	
	printf("%p: %d\n", w+1, *(w+1));
	printf("%p: %d\n", w+2, *(w+2));
	printf("%p: %d\n", w+3, *(w+3));
	printf("%p: %d\n", w+4, *(w+4));
}

